﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb; //连接Access数据库用；

namespace Cashier
{
    public partial class Form_Main : Form
    {
        OleDbConnection objConnection; //数据库连接；
        OleDbCommand sqlcmd; //数据库命令；
        OleDbDataReader reader; //读取的数据；
        string inputBarcode; //输入的条码；

        public Form_Main()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            { //连接数据库；
                //构造连接字符串
                string strConnection = "Provider=Microsoft.Jet.OleDb.4.0;";
                string filePath = Application.StartupPath + "\\..\\..\\goods.mdb";
                strConnection += "Data Source=" + filePath;

                objConnection = new OleDbConnection(strConnection);  //建立连接
                objConnection.Open();  //打开连接

                lab_Barcode.Text = "准备就绪";
            }
            catch (OleDbException er)
            {
                MessageBox.Show(er.Message + "数据库连接出错！");
            }
        }

        double cal;  //当前商品的合计价格；
        private void bConfirm_Click(object sender, EventArgs e)
        {
            inputBarcode = text_Barcode.Text.ToString();
            findBarcodeInDB(inputBarcode);
            if (reader.Read())
            { //这个read调用很重要！不写的话运行时将提示找不到数据
                lab_Barcode.Text = inputBarcode;
                lab_Name.Text = reader["trade_name"].ToString();
                lab_Price.Text = reader["price"].ToString();
                lab_Num.Text = text_Num.Text;
                cal = Convert.ToDouble(lab_Price.Text) * Convert.ToDouble(lab_Num.Text);
                lab_Cal.Text = cal.ToString();

                fillTheTable();
                //MessageBox.Show("OK");
                reader.Close();
            }
            else
                MessageBox.Show("数据库中没有该商品,请检查条码输入是否正确.");
            
        }

        private void fillTheTable()
        { //填表；
            int index = this.dataGridView.Rows.Add();  //添加行并获得行号；
            this.dataGridView.Rows[index].Cells["Col_BC"].Value = reader["barcode"];
            this.dataGridView.Rows[index].Cells["Col_Name"].Value = reader["trade_name"];
            this.dataGridView.Rows[index].Cells["Col_Price"].Value = reader["price"];
            this.dataGridView.Rows[index].Cells["Col_Num"].Value = lab_Num.Text;
            this.dataGridView.Rows[index].Cells["Col_Cal"].Value = cal;
        }

        private int findBarcodeInDB(string strBc)
        {//查找数据库中有没有该商品；
            int i;
            try
            {
                sqlcmd = new OleDbCommand(@"select * from items where barcode='"+inputBarcode+"'" , objConnection);  //sql语句
                reader = sqlcmd.ExecuteReader();  //执行查询
                if (reader == null)
                    i = 0;
                else
                    i = 1;
            }
            catch (OleDbException er)
            {
                MessageBox.Show(er.Message);
                i = -1;
            }
            return i;
        }

        private void but_Sum_Click(object sender, EventArgs e)
        {
            double sum = 0;
            foreach (DataGridViewRow dr in dataGridView.Rows)
            {
                sum += Convert.ToDouble(dr.Cells["col_Price"].Value);
            }
            lab_Sum.Text = sum.ToString() + "元";

            //(new Form_Result()).ShowDialog(); //以模式窗体方式启动；
        }

        private void but_Clear_Click(object sender, EventArgs e)
        {
            lab_Barcode.Text = "准备就绪";
            lab_Name.Text = "";
            lab_Price.Text = "";
            lab_Num.Text = "";
            lab_Cal.Text = "";

            dataGridView.Rows.Clear();  //清空表；

        }
    }
}
