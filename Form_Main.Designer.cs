﻿namespace Cashier
{
    partial class Form_Main
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lab_Cal = new System.Windows.Forms.Label();
            this.lab_Num = new System.Windows.Forms.Label();
            this.lab_Price = new System.Windows.Forms.Label();
            this.lab_Name = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lab_Barcode = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.but_Confirm = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.text_Num = new System.Windows.Forms.TextBox();
            this.text_Barcode = new System.Windows.Forms.TextBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.Col_BC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_Num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col_Cal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.but_Sum = new System.Windows.Forms.Button();
            this.lab_Sum = new System.Windows.Forms.Label();
            this.but_Clear = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.lab_Barcode);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(355, 83);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "当前商品";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lab_Cal);
            this.panel1.Controls.Add(this.lab_Num);
            this.panel1.Controls.Add(this.lab_Price);
            this.panel1.Controls.Add(this.lab_Name);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(143, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(206, 65);
            this.panel1.TabIndex = 1;
            // 
            // lab_Cal
            // 
            this.lab_Cal.AutoSize = true;
            this.lab_Cal.Location = new System.Drawing.Point(152, 27);
            this.lab_Cal.Name = "lab_Cal";
            this.lab_Cal.Size = new System.Drawing.Size(0, 12);
            this.lab_Cal.TabIndex = 7;
            // 
            // lab_Num
            // 
            this.lab_Num.AutoSize = true;
            this.lab_Num.Location = new System.Drawing.Point(53, 49);
            this.lab_Num.Name = "lab_Num";
            this.lab_Num.Size = new System.Drawing.Size(0, 12);
            this.lab_Num.TabIndex = 6;
            // 
            // lab_Price
            // 
            this.lab_Price.AutoSize = true;
            this.lab_Price.Location = new System.Drawing.Point(53, 28);
            this.lab_Price.Name = "lab_Price";
            this.lab_Price.Size = new System.Drawing.Size(0, 12);
            this.lab_Price.TabIndex = 5;
            // 
            // lab_Name
            // 
            this.lab_Name.AutoSize = true;
            this.lab_Name.Location = new System.Drawing.Point(53, 5);
            this.lab_Name.Name = "lab_Name";
            this.lab_Name.Size = new System.Drawing.Size(0, 12);
            this.lab_Name.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(104, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 3;
            this.label5.Text = "合  计：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 2;
            this.label4.Text = "数  量：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 1;
            this.label3.Text = "单  价：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "商品名：";
            // 
            // lab_Barcode
            // 
            this.lab_Barcode.AutoSize = true;
            this.lab_Barcode.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_Barcode.Location = new System.Drawing.Point(17, 31);
            this.lab_Barcode.Name = "lab_Barcode";
            this.lab_Barcode.Size = new System.Drawing.Size(108, 26);
            this.lab_Barcode.TabIndex = 0;
            this.lab_Barcode.Text = "11054126";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.but_Confirm);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.text_Num);
            this.groupBox2.Controls.Add(this.text_Barcode);
            this.groupBox2.Location = new System.Drawing.Point(12, 111);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(355, 61);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "手动输入";
            // 
            // but_Confirm
            // 
            this.but_Confirm.Location = new System.Drawing.Point(274, 24);
            this.but_Confirm.Name = "but_Confirm";
            this.but_Confirm.Size = new System.Drawing.Size(75, 23);
            this.but_Confirm.TabIndex = 5;
            this.but_Confirm.Text = "确  定";
            this.but_Confirm.UseVisualStyleBackColor = true;
            this.but_Confirm.Click += new System.EventHandler(this.bConfirm_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(183, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 4;
            this.label7.Text = "数量";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 3;
            this.label6.Text = "条码";
            // 
            // text_Num
            // 
            this.text_Num.Location = new System.Drawing.Point(218, 25);
            this.text_Num.Name = "text_Num";
            this.text_Num.Size = new System.Drawing.Size(40, 21);
            this.text_Num.TabIndex = 2;
            this.text_Num.Text = "1";
            // 
            // text_Barcode
            // 
            this.text_Barcode.Location = new System.Drawing.Point(41, 25);
            this.text_Barcode.Name = "text_Barcode";
            this.text_Barcode.Size = new System.Drawing.Size(119, 21);
            this.text_Barcode.TabIndex = 0;
            this.text_Barcode.Text = "1100001";
            // 
            // dataGridView
            // 
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Col_BC,
            this.Col_Name,
            this.Col_Price,
            this.Col_Num,
            this.Col_Cal});
            this.dataGridView.Location = new System.Drawing.Point(12, 194);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridView.RowHeadersWidth = 20;
            this.dataGridView.RowTemplate.Height = 23;
            this.dataGridView.Size = new System.Drawing.Size(355, 125);
            this.dataGridView.TabIndex = 2;
            // 
            // Col_BC
            // 
            this.Col_BC.HeaderText = "条码编号";
            this.Col_BC.Name = "Col_BC";
            this.Col_BC.ReadOnly = true;
            this.Col_BC.Width = 80;
            // 
            // Col_Name
            // 
            this.Col_Name.HeaderText = "商品名";
            this.Col_Name.Name = "Col_Name";
            this.Col_Name.ReadOnly = true;
            this.Col_Name.Width = 80;
            // 
            // Col_Price
            // 
            this.Col_Price.HeaderText = "单价";
            this.Col_Price.Name = "Col_Price";
            this.Col_Price.ReadOnly = true;
            this.Col_Price.Width = 60;
            // 
            // Col_Num
            // 
            this.Col_Num.HeaderText = "数量";
            this.Col_Num.Name = "Col_Num";
            this.Col_Num.ReadOnly = true;
            this.Col_Num.Width = 55;
            // 
            // Col_Cal
            // 
            this.Col_Cal.HeaderText = "合计";
            this.Col_Cal.Name = "Col_Cal";
            this.Col_Cal.ReadOnly = true;
            this.Col_Cal.Width = 60;
            // 
            // but_Sum
            // 
            this.but_Sum.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.but_Sum.Cursor = System.Windows.Forms.Cursors.Hand;
            this.but_Sum.Location = new System.Drawing.Point(12, 330);
            this.but_Sum.Name = "but_Sum";
            this.but_Sum.Size = new System.Drawing.Size(140, 38);
            this.but_Sum.TabIndex = 3;
            this.but_Sum.Text = "全 部 结 算";
            this.but_Sum.UseVisualStyleBackColor = false;
            this.but_Sum.Click += new System.EventHandler(this.but_Sum_Click);
            // 
            // lab_Sum
            // 
            this.lab_Sum.AutoSize = true;
            this.lab_Sum.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_Sum.ForeColor = System.Drawing.Color.Lime;
            this.lab_Sum.Location = new System.Drawing.Point(159, 336);
            this.lab_Sum.Name = "lab_Sum";
            this.lab_Sum.Size = new System.Drawing.Size(0, 27);
            this.lab_Sum.TabIndex = 4;
            // 
            // but_Clear
            // 
            this.but_Clear.BackColor = System.Drawing.Color.Red;
            this.but_Clear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.but_Clear.Location = new System.Drawing.Point(261, 330);
            this.but_Clear.Name = "but_Clear";
            this.but_Clear.Size = new System.Drawing.Size(106, 38);
            this.but_Clear.TabIndex = 5;
            this.but_Clear.Text = "全 部 归 零";
            this.but_Clear.UseVisualStyleBackColor = false;
            this.but_Clear.Click += new System.EventHandler(this.but_Clear_Click);
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 379);
            this.Controls.Add(this.but_Clear);
            this.Controls.Add(this.lab_Sum);
            this.Controls.Add(this.but_Sum);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form_Main";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "收银台";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lab_Barcode;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox text_Num;
        private System.Windows.Forms.TextBox text_Barcode;
        private System.Windows.Forms.Label lab_Cal;
        private System.Windows.Forms.Label lab_Num;
        private System.Windows.Forms.Label lab_Price;
        private System.Windows.Forms.Label lab_Name;
        private System.Windows.Forms.Button but_Confirm;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Button but_Sum;
        private System.Windows.Forms.Label lab_Sum;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_BC;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_Num;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col_Cal;
        private System.Windows.Forms.Button but_Clear;
    }
}

